
import org.json4s.native.JsonMethods

package object example {
	implicit val jsonMethods = JsonMethods
	implicit val formats = org.json4s.DefaultFormats
}