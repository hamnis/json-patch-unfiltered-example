package example

import unfiltered.request._
import unfiltered.response._

object Main extends App {
  unfiltered.jetty.Http(1337).plan(new Resources()).run()
}