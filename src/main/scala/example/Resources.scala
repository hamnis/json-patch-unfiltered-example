package example

import javax.servlet.http._

import org.json4s._
import org.json4s.native.JsonMethods._

import unfiltered.request._
import unfiltered.response._
import unfiltered.filter._
import unfiltered.filter.request._

import unfiltered.directives._
import Directives._

import bandaid._

class Resources extends Plan {

  val JsonPatchMimeType = "application/json-patch+json"
  val Intent = Directive.Intent[HttpServletRequest, String]{ case ContextPath(_, path) => path }

  var people = List(Person("Erlend Hamnaberg", 32, "Arktekk", Map("github" -> "http://github.com/hamnis")))
  
  val intent = Intent {
    case Seg("resource.json" :: Nil) => {
      val get = for {
        _ <- GET | HEAD
        _ <- commit(Accepts.Json)
      } yield {
        JsonContent ~> ResponseString(pretty(render(toJson)))
      }
      val options = for {
        _ <- OPTIONS
      } yield {
        Ok ~> Allow("HEAD, GET, OPTIONS, PATCH") ~> AcceptPatch(JsonPatchMimeType)
      }
      val patch = for {
        _ <- PATCH
        _ <- commit(when{case RequestContentType(JsonPatchMimeType) => ()}.orElse(UnsupportedMediaType))
        is <- inputStream
      } yield {
        val original = toJson
        val patch = Patch.parse(is)
        val patched = patch(original)
        people = toList(patched)
        NoContent
      }
      get | patch | options
    }
  }

  def toJson: JObject = {    
    JObject(List(JField("people", Extraction.decompose(people))))
  }

  def toList(obj: JValue): List[Person] = {    
    val JArray(list) = obj \ "people"
    list.map(_.extract[Person])
  }
}

object AcceptPatch extends HeaderName("Accept-Patch")

case class Person(name: String, age: Int, company: String, links: Map[String, String])