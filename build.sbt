organization := "net.hamnaberg.example"

name := "unfiltered-json-patch-example"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.10.2"

libraryDependencies += "net.hamnaberg.json" %% "bandaid" % "0.1.0-SNAPSHOT"

libraryDependencies += "net.databinder" %% "unfiltered-directives" % "0.6.8"

libraryDependencies += "net.databinder" %% "unfiltered-jetty" % "0.6.8"

libraryDependencies += "net.databinder" %% "unfiltered-filter" % "0.6.8"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.2.2"
